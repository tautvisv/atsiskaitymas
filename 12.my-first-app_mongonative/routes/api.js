﻿function initialize(app) {
    app.get('/api/categories', app.services.category.list.bind(app.services.category));
	app.get('/api/categories/:id', app.services.category.get.bind(app.services.category));
	app.post('/api/categories', app.services.category.create.bind(app.services.category));
	app.post('/api/categories/:name', app.services.category.update.bind(app.services.category));
	app.post('/api/categories/:name/:active', app.services.category.update.bind(app.services.category));
	app.put('/api/categories/:name', app.services.category.create.bind(app.services.category));
	app.delete('/api/categories/:name', app.services.category.delete.bind(app.services.category));

    app.get('/api/images', app.services.image.list.bind(app.services.image));
    app.get('/api/images/:id', app.services.image.get.bind(app.services.image));
    app.get('/api/favorites', app.services.image.listFavorites.bind(app.services.image));
    app.get('/api/imageCategories/:id', app.services.image.listFavorites.bind(app.services.image));
    app.get('/api/allimages/:param', app.services.image.listParam.bind(app.services.image));
    app.put('/api/images/:name', app.services.image.create.bind(app.services.image));
 //   app.put('/api/images/:act/:sort', app.services.image.listByAttr.bind(app.services.image));
    app.get('/api/images/category/:category/tags/:tags/sort/:sort', app.services.image.listByAttr.bind(app.services.image));
    app.post('/api/images', app.services.image.create.bind(app.services.image));
    app.post('/api/images/:name', app.services.image.update.bind(app.services.image));
    app.delete('/api/images/:name', app.services.image.delete.bind(app.services.image));
    app.delete('/api/favorites/:ids', app.services.image.deleteFavorites.bind(app.services.image));

    
    app.get('/api/tags', app.services.tag.list.bind(app.services.tag));
	app.post('/api/tags', app.services.tag.create.bind(app.services.tag));
	app.post('/api/tags/:name', app.services.tag.update.bind(app.services.tag));
	app.put('/api/tags/:name', app.services.tag.create.bind(app.services.tag));
	app.delete('/api/tags/:name', app.services.tag.delete.bind(app.services.tag));
}

module.exports = {
    initialize: initialize
};