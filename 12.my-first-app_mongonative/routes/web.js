﻿function index(req, res){
    res.render('index', { title: 'Express' });
};

function helloWorld(req, res) {
    res.send('Hello world!');
};

function initialize(app) {
    app.get('/', index);
    app.get('/hello', helloWorld);

    app.get('/download-image/:id', app.services.image.download.bind(app.services.image));
    app.get('/download-all-images/', app.services.image.downloadAll.bind(app.services.image));
}

module.exports = {
    initialize: initialize
};