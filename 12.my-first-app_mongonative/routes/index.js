var web = require('./web'),
    api = require('./api');

module.exports = {
    web: web,
    api: api
};