
/**
 * Module dependencies.
 */

var routes = require('./routes');
var http = require('http');
var path = require('path');
var log4js = require('log4js');
var events = require('events');
var express = require('express');

var app = express();
app.events = new events.EventEmitter();

log4js.loadAppender('file');
log4js.configure('config/logging.json');

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.json());


app.use(function(req, res, next) {
	log4js.getLogger('request').info(req.url);
    next();
});

app.use(express.bodyParser());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(err, req, res, next) {
    log4js.getLogger().error(err);

    res.send(500, {
        success: false,
        message: "Internal Server Error"
    });
});

// Initialize services
app.services = require('./public/services')(app);

// Initialize routes
routes.api.initialize(app);
routes.web.initialize(app);

http.createServer(app).listen(app.get('port'), function(){
    log4js.getLogger().info('Express server listening on port ' + app.get('port'));
});

process.on('uncaughtException', function (err) {
    log4js.getLogger().error(err);
});