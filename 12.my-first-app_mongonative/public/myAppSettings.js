﻿var myApp = angular.module('myApp', ['ngRoute','angularFileUpload'])
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/categories.html', {
        templateUrl: 'partials/categories/content.shtml',
        controller: 'categoriesController'
    }).when('/favorites.html', {
        templateUrl: 'partials/favorites/content.shtml',
        directive: 'downloadDirective',
        controller: 'favoritesController'
    }).when('/photos.html', {
        templateUrl: 'partials/photos/content.shtml',
        controller: 'photosController'
    }).when('/tags.html', {
        templateUrl: 'partials/tags/content.shtml',
        controller: 'tagsController'
    }).otherwise({
        redirectTo: '/categories.html'
    });
}])
.run(function ($rootScope) {
    $rootScope.menus = [{name:'Favorites',image:'icon-heart'},{name:'Categories',image:'icon-drawer'},{name:'Photos',image:'icon-images'},{name:'Tags',image:'icon-tag'}];
});