﻿myApp.controller('allPhotosController', ['$scope', '$rootScope', '$q', '$upload', 'photosService', function ($scope, $rootScope, $q, $upload, photosService, tagsService, categoriesService) {

    $scope.points = [1, 2, 3, 4, 5];

    $scope.save = function (name, item, closeModal) {
        if (item.tags && item.tags.length > 0 && item.categories && item.categories.length > 0 && item.title) {
            $q.all([
            photosService.post(name, item)
            ]).then(function (data) {
                if (closeModal && typeof closeModal === 'function') {
                    $scope.getAll($rootScope.value);
                    closeModal();
                }
            });
        } else {
            alert("Category, tag, title or file is not set.");
        }
    }

    $scope.selectByAttr = function () {
        $q.all([
        photosService.getByAttr($scope.selectedcategory, $scope.selectedtag, $scope.selectsort)
        ]).then(function (data) {
            $scope.photos = data[0];
        });

    };

    $scope.getAll = function (params) {
        $q.all([
        photosService.getByParams(params),
        ]).then(function (data) {
            $scope.photos = data[0];
        });
    }


    $scope.addToFavorites = function (item) {
        if (!item.favorites) {
            item.favorites = true;
        } else {
            item.favorites = false;
        }
        $q.all([
                   photosService.post(item.name, item)
        ]).then(function (data) {
            if (item.favorites) {
                alert("Photo is added to favorites");
            } else {
                alert("Photo is removede from favorite");
            }
        });
    }

    $scope.addVote = function (photo, num) {
        photo.votes.push(num);
        $q.all([
            photosService.post(photo.name, photo)
        ]).then(function (data) {
        });
    }

    $scope.change = function (item) {
        item.active = !item.active;
        $scope.save(item.name, item);
    }

    $scope.$on('search', function() {
                $scope.getAll($rootScope.value);
            });

    $scope.getPhotos = function(category){
        $q.all([
        photosService.getByAttr(category),
        ]).then(function (data) {
            $scope.photos = data[0];
        });
        }
    $scope.getAll($rootScope.value);

}]);
