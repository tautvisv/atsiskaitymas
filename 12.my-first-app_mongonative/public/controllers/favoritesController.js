﻿myApp
.controller('favoritesController', ['$scope', '$rootScope', '$q', '$upload', 'favoritesService', 'tagsService', function ($scope, $rootScope, $q, $upload, favoritesService, tagsService) {
    $rootScope.title = 'Favorites';

    $scope.disselectAll = function () {
        $scope.removeable = [];
    };

    $scope.isSelected = function (item) {
        if ($scope.removeable.indexOf(item) !== -1) {
            return "selected";
        }
        return "";
    };

    $scope.selectAll = function () {
        $scope.disselectAll();
        $scope.favorites.forEach(function (item) {
            $scope.addToRemove(item._id);
        });
    };

    $scope.remove = function (photosList) {
        var para = [];
        photosList.forEach(function (item) {
            para.push('ids[]=' + item || 0);
        });
        if (para.length > 0) {
            $q.all([
                favoritesService.deleteF(para.join('&'))
            ]).then(function (data) {
                $scope.getAll();
            });
        }
    }

    $scope.addToRemove = function (item) {
        if ($scope.removeable.indexOf(item) === -1) {
            $scope.removeable.push(item);
        } else {
            $scope.removeable.splice($scope.removeable.indexOf(item), 1);
        }
    }

    $scope.addVote = function (photo, num) {
        photo.votes.push(num);
        $q.all([
            favoritesService.update(photo.name, photo)
        ]).then(function (data) {
        });
    }

    $scope.fileName = function (link) {
        var regex = /([0-9]{2}).(jpg$)+/g;
        var matched = regex.exec(link);
        return matched[1];
    }

    $scope.getAll = function () {
        $q.all([
        favoritesService.get(),
        tagsService.get()
        ]).then(function (data) {
            $scope.favorites = data[0];
            $scope.tags = data[1];
            $rootScope.favoritesLength = data[0].length;
        });
    }
    $scope.addToFavorites = function (item) {
        if (!item.favorites) {
            item.favorites = true;
        } else {
            item.favorites = false;
        }
        $q.all([
                   favoritesService.update(item.name, item)
        ]).then(function (data) {
            if (item.favorites) {
                alert("Photo is added to favorites");
            } else {
                alert("Photo is removede from favorite");
            }
        });
    }

    $scope.downloadSelected = function () {
        $scope.$broadcast("downloadFile", $scope.favorites[0].link);
    }

    $scope.disselectAll();
    $scope.getAll();
}]);
