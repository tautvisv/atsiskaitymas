﻿myApp
        .controller('photosController', ['$scope', '$rootScope', '$q', '$upload', 'photosService', 'tagsService', 'testService', function ($scope, $rootScope, $q, $upload, photosService, tagsService, categoriesService) {
            $rootScope.title = 'Photos';
            $scope.points = [1,2,3,4,5];

            $scope.sortas = [
                { name: "Ascending", _id: 1 },
                { name: "Descending", _id: 2 },
                { name: "Active", _id: 3 },
                { name: "Inactive", _id: 4 }
            ];

            $scope.save = function (name, item, closeModal) {
                if (item.tags && item.tags.length > 0 && item.categories && item.categories.length > 0 && item.title ) {
                    $q.all([
                    photosService.post(name, item)
                    ]).then(function (data) {
                        if (closeModal && typeof closeModal === 'function') {
                            $scope.getAll();
                            closeModal();
                        }
                    });
                } else {
                    alert("Category, tag, title or file is not set.");
                }
            }

            $scope.create = function (name, item, closeModal) {
                console.log($scope.file);
                if (item.tags && item.tags.length > 0 && item.categories && item.categories.length > 0 && item.title && $scope.file) {
                    $q.all([
                    photosService.put(item.name, item, $scope.file)
                    ]).then(function (data) {
                        if (closeModal && typeof closeModal === 'function') {
                            $scope.getAll();
                            closeModal();
                        }
                    });
                } else {
                    alert("Category, tag, name or photo is not set.");
                }
            }

            $scope.deleteF = function (item) {
                var box = confirm("Are you sure you want to delete?");
                if (box) {
                    $q.all([
                    photosService.deleteF(item.name, item),
                    ]).then(function (data) {
                        $scope.photos.splice($scope.photos.indexOf(item),1);
                    });
                }
            }
            /*
            $scope.toggleCheck = function (id, list) {
                if (list.indexOf(id) === -1) {
                    list.push(id);
                } else {
                    list.splice(list.indexOf(id), 1);
                }
            };*/

            $scope.selectByAttr = function () {
                $q.all([
                photosService.getByAttr($scope.selectedcategory, $scope.selectedtag, $scope.selectsort)
                ]).then(function (data) {
                    $scope.photos = data[0];
                });

                //      $scope.list = Enumerable.From($scope.photos).Where(function (photo) { return photo._id != "200" });
                /*
                $scope.list = [];
                $scope.photos.forEach(function (photo) {
                    photo.tags.forEach(function (item) {
                        if (item._id.indexOf($scope.selectedtag._id) != -1) {
                                    $scope.list.push(photo);
                        }
                    });
                });*/
            };

            $scope.getAll = function () {
                $q.all([
                photosService.get(),
                tagsService.get(),
                categoriesService.get(),
                ]).then(function (data) {
                    $scope.photos = data[0];
                    $scope.tags = data[1];
                    $scope.categories = data[2];
                });
            }


            $scope.addVote = function (photo, num) {
                photo.votes.push(num);
                $q.all([
                    photosService.post(photo.name, photo)
                ]).then(function (data) {
                });
            }

            $scope.change = function (item) {
                item.active = !item.active;
                $scope.save(item.name, item);
            }

            $scope.onFileSelect = function($files) { 
                console.log($files);
                $scope.file = $files[0];
            }

            $scope.getAll();

        }]);
