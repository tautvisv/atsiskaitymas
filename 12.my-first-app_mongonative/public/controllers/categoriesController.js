﻿myApp
        .controller('categoriesController', ['$scope', '$rootScope', '$q', 'testService', function ($scope, $rootScope, $q, testService) {
            $rootScope.title = "Categories";

            $scope.isActive = function(active){
                if (active){
                    return "active";
                    }
                    return "";
                }
            $scope.deleteF = function (item) {
                var box = confirm("Are you sure you want to delete?");
                if (box) {
                    $q.all([
                    testService.deleteF(item.name, item),
                    ]).then(function (data) {
                        $scope.categories.splice($scope.categories.indexOf(item),1);
                    });
                }
            }

            $scope.change = function (item) {
                item.active = !item.active;
                $scope.save(item.name, item);
            }

            $scope.create = function (name, item, closeModal) {
                name = name || item.name;
                $q.all([
                testService.put(name, item)
                ]).then(function (data) {
                    $scope.categories.push(item);
                    if (closeModal && typeof closeModal === 'function') {
                        closeModal();
                    }
                });

            }



            $scope.getAll = function () {
                $q.all([
                testService.get(),
                ]).then(function (data) {
                    $scope.categories = data[0];
                });
            }

            $scope.save = function (name, categoryData, closeModal) {
                $q.all([
                testService.post(name, categoryData),
                ]).then(function (data) {
                    if (closeModal && typeof closeModal === 'function') {
                        $scope.getAll();
                        closeModal();
                    }
                });
            }
            $scope.getAll();
        }]);