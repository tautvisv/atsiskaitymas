﻿myApp
        .controller('tagsController',[ '$scope', '$rootScope','$q', 'tagsService', function ($scope, $rootScope, $q, tagsService) {
            $rootScope.title = 'Tags';

            $scope.deleteF = function (item) {
                var box = confirm("Are you sure you want to delete?");
                if (box) {
                    $q.all([
                    tagsService.deleteF(item.name,item),
                    ]).then(function (data) {
                        $scope.tags.splice($scope.tags.indexOf(item),1);
                    });
                }
            }

            $scope.create = function (name,item,closeModal) {
                if (!name){
                    name = item.name;
                }
                $q.all([
                tagsService.put(name,item)
                ]).then(function(data){
                    $scope.tags.push(item);
                    if(closeModal && typeof closeModal === 'function'){
                        closeModal();
                }
                });

                }

            $scope.getAll = function () { 
                $q.all([
                tagsService.get(),
                ]).then(function(data){
                    $scope.tags = data[0];
                });
            }

            $scope.save = function (name, tagData,closeModal) { 
                $q.all([
                tagsService.post(name, tagData),
                ]).then(function(data){
                    if(closeModal && typeof closeModal === 'function'){
                        $scope.getAll();
                        closeModal();
                }
                });
            }
            $scope.getAll();
        }]);