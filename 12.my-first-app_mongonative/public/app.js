﻿var myApp = angular.module('myApp', ['ngRoute','angularFileUpload'])
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        controller: 'allPhotosController',
        templateUrl: 'partials/main.html'
    }).when('/fresh', {
        controller: 'allPhotosController',
        templateUrl: 'partials/main.html'
    }).when('/top', {
        controller: 'allPhotosController',
        templateUrl: 'partials/main.html'
    }).when('/popular', {
        controller: 'allPhotosController',
        templateUrl: 'partials/main.html'
    }).otherwise({
        redirectTo: '/'
    });
}])
.run(function ($rootScope) {
    $rootScope.value = "nan";
     $rootScope.avgFunction = function (array) {
                var i, sum = 0;
                if (array.length) {
                    for (i = 0; i < array.length; i++) {
                        sum += array[i];
                    }
                    sum = Math.round(sum / array.length);
                }
                return sum;
            }

     $rootScope.search = function (atr) {
         $rootScope.value = atr;
     }

     $rootScope.searchM = function (atr) {
         $rootScope.value = atr;
         $rootScope.$broadcast("search");
     };


     $rootScope.isPressed = function (value) {
        if (value === $rootScope.value) {
            return "active";
        }

        return "";
    };

    $rootScope.active = function (item) {

        if (item.active) {
            return true; // this will be listed in the results
        }

        return false; // otherwise it won't be within the results
    };

    $rootScope.changeFavorite = function (item) {
        console.log(item);
        item.favorites = !item.favorites;
    }

    $rootScope.isFavorite = function (item) {
        if (item) {
            return "marked"; // this will be listed in the results
        }

        return ""; // otherwise it won't be within the results
    };

    $rootScope.index = 0;
});