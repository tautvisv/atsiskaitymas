﻿myApp.directive('modalEditData', function($compile) {
    return {
        restrict: 'A',
        scope: {
            item : '=itemData',
            save : '=saveFunction'
        },
        link: function(scope, element, attrs) {
            var options = scope.$eval(attrs.modalEditData);
            
            element.on('click', function(e) {
                e.preventDefault();

                $(this).openModal({
                    templateId: 'modal-edit-data',
                    title: options.title,
                    onLoad: function() {
                        var modal = this,
                            contentContainer = modal.contentContainer,
                            html = contentContainer.html();

                        contentContainer.html($compile(html)(scope));
                        scope.$apply();
                        scope.closeModal = function(){
                                modal.close();
                            };
                    }
                });
            });
        }
    };
});