﻿myApp.directive('modalFavoritesEdit', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        scope: {
            itemInfo: '=itemInfo',
            list: '=list',
            addVote: '=addVote',
            index: '=index',
            addToFavorites: '=addToFavorites'
        },
        link: function (scope, element, attrs) {
            var options = scope.$eval(attrs.modalFavoritesEdit);
            element.on('click', function (e) {
                e.preventDefault();

                scope.points = [1, 2, 3, 4, 5];

                scope.resetIndex = function (indexas) {
                    index = indexas;
                };
                scope.mano = function (value) {
                    var rez = scope.index + value;
                    if (rez >= 0 && rez < scope.list.length) {
                        scope.index = rez;
                    }
                    console.log(scope.index);
                    return scope.list[scope.index];
                };
                scope.avgFunction = function (array) {
                    var i, sum = 0;
                    if (array.length) {
                        for (i = 0; i < array.length; i++) {
                            sum += array[i];
                        }
                        sum = Math.round(sum / array.length);
                    }
                    return sum;
                }

                $(this).openModal({
                    templateId: 'modal-photo',
                    title: options.title,
                    onLoad: function () {
                        var modal = this,
                            contentContainer = modal.contentContainer,
                            html = contentContainer.html();
                        
                        scope.closeModal = function () {
                            modal.close();
                        };
                        contentContainer.html($compile(html)(scope));
                        scope.$apply();
                    }
                });
            });
        }
    };
}]);