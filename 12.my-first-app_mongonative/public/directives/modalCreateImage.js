﻿myApp.directive('modalCreateImage', function($compile) {
    return {
        restrict: 'A',
        scope: {
            item : '=item',
            tags : '=tagsData',
            categories : '=categoriesData',
            onFileSelect: '=addFileFunction',
            save : '=saveFunction'
        },
        link: function(scope, element, attrs) {
            var options = scope.$eval(attrs.modalCreateImage);
            
            element.on('click', function(e) {
                e.preventDefault();

                $(this).openModal({
                    templateId: 'modal-edit-data',
                    title: options.title,
                    onLoad: function() {
                        var modal = this,
                            contentContainer = modal.contentContainer,
                            html = contentContainer.html();

                        
                        scope.getIDs = function (items) {
                            var obj = [];
                            if (items && items.length > 0)
                                items.forEach(function (item) {
                                    obj.push(item._id);
                                });
                            return obj;
                        };

                        contentContainer.html($compile(html)(scope));
                        scope.$apply();
                        scope.toggleCheck = function (id, list) {
                            if (list.indexOf(id) === -1) {
                                list.push(id);
                            } else {
                                list.splice(list.indexOf(id), 1);
                            }
                            console.log(list);
                        }

                        scope.closeModal = function () {
                                modal.close();
                            };
                    }
                });
            });
        }
    };
});