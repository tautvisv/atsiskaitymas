﻿myApp.directive('categoryitemDirective', function () {
            return {
                restriction: 'A',
                templateUrl: 'directives/itemsHTML/categoryList.html'

            }
        }

);

/**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 */