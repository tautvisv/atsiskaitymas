﻿myApp.factory('tagsService', function($q,$http) {
    return {
        get: function () {

            var deferred = $q.defer();


            $http({ method: 'GET', url: '/api/tags' }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    if (data.success && Array.isArray(data.data)) {
                        objs = data.data;
                }
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });

            
            return deferred.promise;
        },
        post: function (name,tagParams) {
            
            var deferred = $q.defer();

            $http({ method: 'POST', url: '/api/tags/'+name, data:tagParams }).
                success(function (data, status, headers, config) {
                    var objs = [];
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        },
        put: function (name,item) {
            
            var deferred = $q.defer();

            $http({ method: 'PUT', url: '/api/tags/'+name, data:item }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        },

        deleteF: function (name,categoryParams) {
            
            var deferred = $q.defer();

            $http({ method: 'DELETE', url: '/api/tags/'+name}).
                success(function (data, status, headers, config) {
                    deferred.resolve(data,status);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        }
    };
        
});
