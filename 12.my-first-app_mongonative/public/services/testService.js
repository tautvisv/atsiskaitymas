﻿myApp.factory('testService', function ($q, $http) {
    return {
        get: function () {

            var deferred = $q.defer();


            $http({ method: 'GET', url: '/api/categories' }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    if (data.success && Array.isArray(data.data)) {
                        objs = data.data;
                }
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });

            
            return deferred.promise;
        },
        post: function (name,categoryParams) {
            
            var deferred = $q.defer();

            $http({ method: 'POST', url: '/api/categories/'+name, data:categoryParams }).
                success(function (data, status, headers, config) {
                    var objs = [];
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        },

        put: function (name,categoryParams) {
            
            var deferred = $q.defer();

            $http({ method: 'PUT', url: '/api/categories/'+name, data:categoryParams }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        },

        deleteF: function (name,categoryParams) {
            
            var deferred = $q.defer();

            $http({ method: 'DELETE', url: '/api/categories/'+name}).
                success(function (data, status, headers, config) {
                    var objs = [];
                    deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        }

    };

});
