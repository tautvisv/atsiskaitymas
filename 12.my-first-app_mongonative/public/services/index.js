﻿var CategoryService = require('./category'),
    ImageService = require('./image'),
    TagService = require('./tag');

module.exports = function(app) {
    var tagService = new TagService(app);
    var categoryService = new CategoryService(app);
    return {
        category: categoryService,
        image: new ImageService(app, tagService,categoryService),
        tag: tagService
    }
};