﻿myApp.factory('favoritesService', function($q,$http) {

    return {

        update: function (name,imageParm) {
            
            var deferred = $q.defer();

            $http({ method: 'POST', url: '/api/images/'+name, data:imageParm }).
                success(function (data, status, headers, config) {
                    var objs = [];
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        },
        
        deleteF: function (list) {
            
            var deferred = $q.defer();

            $http({ method: 'DELETE', url: '/api/favorites/'+list}).
                success(function (data, status, headers, config) {
                    var objs = [];
                    deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        },

        get: function () {

            var deferred = $q.defer();


            $http({ method: 'GET', url: '/api/favorites' }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    if (data.success && Array.isArray(data.data)) {
                        objs = data.data;
                }
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });

            
            return deferred.promise;
        }
}
        
        
});
