﻿var util = require('util');
var myDatabaseName = 'test';
var mongo = require('mongodb'),
    BSON = mongo.BSONPure,
    server = new mongo.Server('localhost', mongo.Connection.DEFAULT_PORT, {
        auto_reconnect: true,
        safe: false
    }),
    db = new mongo.Db(myDatabaseName, server);

db.open(function(err, db) {
    if (!err) {
        console.log("Connected to '"+myDatabaseName+"' database.");
    }
});

function ServiceBase(app) {

    var self = this;

    self.fs = require('fs'); 
    self.linq = require('linq');
    self.step = require('step');
    self.jsonResponse = require('./jsonResponse');
    self.log4js = require('log4js');
    self.app = app;

    self.getDatabase = function(){
        return db;
        }

    self.getCollection = function(err) {
        if (err) {
            throw err;
        }

        db.collection(self.collectionName, this);
// Alternative:
//        this(null, db.collection(self.collectionName));
    };

    self.getAllItems = function (err, collection) {
        if (err) {
            throw err;
        }

        collection.find().toArray(this);
    };

    self.sendJson = function(res, data, message) {
        self.jsonResponse.send(res, true, data, message);
    };

    self.sendError = function(res, message, err, isJson) {
        if (err) {
            self.log4js.getLogger().error(message);
            self.log4js.getLogger().error(err);
        } else {
            self.log4js.getLogger('validation').warn(message);
        }

        if (isJson) {
            self.jsonResponse.send(res, false, {}, message);
        } else {
            res.send(500, message);
        }
    };

    self.forceDownload = function(res, fileName, fileSource, contentType) {
        if (fileSource) {
            res.set('Content-Type', contentType);
            res.attachment(fileName);
            res.send(fileSource);
        } else {
            res.download(fileName);
        }
    };

    self.endFlowForceDownload = function(err, fileName, fileSource, contentType) {
        if (err) {
            var message = err instanceof Error ? err.message : err,
                error = err instanceof Error ? err : null;
            self.sendError(this.res, message, error, false);
            return;
        }

        self.forceDownload(this.res, fileName, fileSource, contentType);
    };

    self.endFlow  = function(err, results) {
        if (err) {
            var message = err instanceof Error ? err.message : err,
                error = err instanceof Error ? err : null;
            self.sendError(this.res, message, error, true);
            return;
        }
        if(results)
            var a = JSON.stringify(results);
        self.sendJson(this.res, results, this.message);
    };

    self.startFlow = function(req, res, flow) {
        if (!util.isArray(flow)) {
            flow = [];
        }
        flow.unshift(function() {
            this.req = req;
            this.res = res;

            this(null);
        });

        self.step.apply(this, flow);
    };

    self.deleteItem = function (err, item, collection) {
        if (err) {
            throw err;
        }
        var id = item._id.toString();
        var next = this;
        collection.remove({"_id": new BSON.ObjectID(id) }, function(err) {
            if (err) {
                next(err);
                return;
            }
            //2 parameteras collection
            next(null, true);
        });

    };

    self.createItem = function(err, item, collection) {
        if (err) {
            throw err;
        }

        var next = this;
        collection.insert(item, { safe: true }, function(err, result) {
            if (err) {
                next(err);
            } else {
                next.message = "Item created successfully";
                next(null, item);
            }
        });
    };

    self.updateItem = function(err, item, collection) {
        if (err) {
            throw err;
        }

        var next = this;
        collection.update({ "_id": item._id }, item, { safe: true }, function(err) {
            if (err) {
                next(err);
            } else {
                next(null, collection, item);
            }
        });

    };

    self.getItemFromRequest = function(err, collection) {
        if (err) {
            throw err;
        }

        var item = {};
        self.mapRequestItem(this.req, this.res, item, collection, this);
    };

    self.createItemFromRequest = function(err, collection) {
        if (err) {
            throw err;
        }

        var item = {};
        self.mapNewRequestItem(this.req, this.res, item, collection, this);
    };

    self.mapItemFromRequest = function(err, item, collection) {
        if (err) {
            throw err;
        }

        var newItem = util._extend({}, item);
        self.mapRequestItem(this.req, this.res, newItem, collection, this);
    };

    self.validateItem = function(err, item, collection) {
        if (err) {
            throw err;
        }

        self.validate(this.req, item, collection, this);
    };

    ServiceBase.prototype.validateItemUpdate = function(err, item, collection) {
        
        if (err) {
            throw err;
        }

        self.validateUpdate(this.req, item, collection, this);
    };

    self.beforeSaveItem = function(err, item, collection) {
        if (err) {
            throw err;
        }

        self.beforeSave(this.req, item, collection, this);
    };

    self.afterItemUpdated = function(err, collection, item) {
        if (err) {
            throw err;
        }

        self.afterUpdate(collection, item, this);
    };
}

ServiceBase.prototype.getCurrentItem = function(err, collection) {
        if (err) {
            throw err;
        }

        var id = this.req.params.id,
            next = this;
       //     id = parseInt(id,10);
        if (!id) {
            this("Id is not set.");
        }

        collection.findOne({"_id": new BSON.ObjectID(id) }, function(err, item) {
            if (err) {
                next(err);
            }
            if (item) {
                next(null, item, collection);
                return;
            }
            next("Item with id " + id + " doesn't exist.");
        });
    };


ServiceBase.prototype.afterUpdate = function(items, item, next) {
    next(null, item, items);
};

ServiceBase.prototype.beforeSave = function(req, item, items, next) {
    next(null, item, items);
};

ServiceBase.prototype.validate = function(req, item, items, next) {
    next(null, item, items);
};

ServiceBase.prototype.validateUpdate = function(req, item, items, next) {
    next(null, item, items);
};

ServiceBase.prototype.mapRequestItem = function(req, res, item, items, next) {
    next(null, item, items);
};

ServiceBase.prototype.mapNewRequestItem = function(req, res, item, items, next) {
    next(null, item, items);
};

ServiceBase.prototype.create = function(req, res) {
    var self = this;

    self.startFlow(req, res, [
        self.getCollection,
        self.createItemFromRequest,
        self.validateItem,
        self.beforeSaveItem,
        self.createItem,
        self.endFlow
    ]);
};

ServiceBase.prototype.update = function(req, res) {
    var self = this;

    self.startFlow(req, res, [
        self.getCollection,
        self.getCurrentItem,
        self.mapItemFromRequest,
        self.validateItemUpdate,
        self.beforeSaveItem,
        self.updateItem,
        self.afterItemUpdated,
        self.endFlow
    ]);
};

ServiceBase.prototype.delete = function(req, res) {
    var self = this;

    self.startFlow(req, res, [
        self.getCollection,
        self.getCurrentItem,
        self.deleteItem,
        self.endFlow
    ]);
};

ServiceBase.prototype.list = function(req, res) {
    var self = this;

    self.startFlow(req, res, [
        self.getCollection,
        self.getAllItems,
        self.endFlow
    ]);
};

ServiceBase.prototype.get = function(req, res) {
    var self = this;

    self.startFlow(req, res, [
        self.getCollection,
        self.getCurrentItem,
        self.endFlow
    ]);
};

module.exports = ServiceBase;