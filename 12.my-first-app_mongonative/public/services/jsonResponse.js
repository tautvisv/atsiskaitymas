﻿module.exports = {
    send: function(res, success, data, message) {
        res.send({
            success: success,
            data: data,
            message: message
        });
    }
}