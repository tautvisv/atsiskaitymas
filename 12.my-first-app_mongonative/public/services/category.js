﻿var util = require("util"),
    BSON = require('mongodb').BSONPure,
    ServiceBase = require('./base'),
    CategoryService = function (app) {
        var self = this;
        this.collectionName = 'categories';
        this.GetTags = function (ids, callback) {
            self.getDatabase().collection(self.collectionName, function (err, collection) {
                collection.find({ "_id": { $in: ids } },{name:1}).toArray(callback);
            });

        };

        ServiceBase.apply(this, arguments);
        
    };

util.inherits(CategoryService, ServiceBase);

CategoryService.prototype.getCurrentItem = function (err, collection) {
    if (err) {
        throw err;
    }

    var name = this.req.params.name,
        next = this;
    //     id = parseInt(id,10);
    if (!name) {
        this("Name is not set.");
    }

    collection.findOne({ "name": name }, function (err, item) {
        if (err) {
            next(err);
        }
        if (item) {
            next(null, item, collection);
            return;
        }
        next("Item with name " + name + " doesn't exist.");
    });
};

CategoryService.prototype.mapNewRequestItem = function (req, res, item, collection, next) {
    item.name = req.body.name;
    item.active = req.body.active;
    item.images = [];
    next(null, item, collection);
}

CategoryService.prototype.mapRequestItem = function (req, res, item, collection, next) {
    //   item.name = req.body.name;
    item.name = req.body.name;
    item.active = req.body.active;
    next(null, item, collection);
};

CategoryService.prototype.validate = function (req, item, collection, next) {
    var self = this;

    if (!item.name) {
        next("Category name must be set.");
        return;
    }

    collection.find({
        "name": item.name
    }).count(function (err, count) {
        if (err) {
            next(err);
            return;
        }

        if (count > 0) {
            next("Category " + item.name + " already exists.");
            return;
        }

        next(null, item, collection);
    });
};

CategoryService.prototype.validateUpdate = function (req, item, collection, next) {
    var self = this;

    if (!item.name) {
        next("Category name must be set.");
        return;
    }
    next(null, item, collection);

}

CategoryService.prototype.afterUpdate = function (collection, item, next) {
    var self = this;

    self.app.events.emit('categoryUpdated', item);

    next(null, item, collection);
};

module.exports = CategoryService;