﻿myApp.factory('photosService', function($q,$http,$upload) {

    return {
        getByParams: function (params) {
            var deferred = $q.defer();

           // $http({ method: 'GET', url: '/api/'+params }).
            $http({ method: 'GET', url: '/api/allimages/'+params }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    if (data.success && Array.isArray(data.data)) {
                        objs = data.data;
                }
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });

            
            return deferred.promise;
        },

        getByAttr: function (category,tag,sort) {
            var dataToPost = "";
                deferred = $q.defer();
          if (category)
            dataToPost += 'category/' + category._id;
          else dataToPost += 'category/0';
          if(tag)
            dataToPost += '/tags/'+tag._id;
          else dataToPost += '/tags/0';
          if (sort)
            dataToPost += '/sort/'+sort._id
          else dataToPost += '/sort/0';

            console.log(dataToPost);
            $http({ method: 'GET', url: '/api/images/'+ dataToPost }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    if (data.success && Array.isArray(data.data)) {
                        objs = data.data;
                }
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });

            
            return deferred.promise;
        },

        get: function () {

            var deferred = $q.defer();


            $http({ method: 'GET', url: '/api/images' }).
                success(function (data, status, headers, config) {
                    var objs = [];
                    if (data.success && Array.isArray(data.data)) {
                        objs = data.data;
                }
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });

            
            return deferred.promise;
        },
        post: function (name,imageParm) {
            
            var deferred = $q.defer();

            $http({ method: 'POST', url: '/api/images/'+name, data:imageParm }).
                success(function (data, status, headers, config) {
                    var objs = [];
                deferred.resolve(objs);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        },
        put: function (name,item,file) {
            
            var deferred = $q.defer();

            $upload.upload({
                url: '/api/images/'+name,
                method: 'PUT',
                data: item,
                file: file,
            }).success(function(data, status, headers, config) {
                deferred.resolve(data);    
            });

            return deferred.promise;
        },

        deleteF: function (id,categoryParams) {
            
            var deferred = $q.defer();

            $http({ method: 'DELETE', url: '/api/images/'+id}).
                success(function (data, status, headers, config) {
                    deferred.resolve(data,status);
                }).error(function (data, status, headers, config) {
                    deferred.reject(data, status);
            });
            return deferred.promise;
        }
    };
        
});
