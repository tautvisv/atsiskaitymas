﻿var util = require("util"),
    BSON = require('mongodb').BSONPure,
    fs = require('fs'),
    ServiceBase = require('./base'),
    avgFunction = function(array){
        var avg = 0, i;
        if (array.length){
            for (i = 0; i < array.length; i++){
                avg += array[i];
            }
            avg = avg/array.length;
        }
        return avg;
    },
    convertArrayToBSONArray = function (items) {
        var tmp = [];
        if (items)
        items.forEach(function (item) {
            tmp.push(new BSON.ObjectID(item));
        });
        return tmp;
    },
    parseURL = function(url){
            var reg = /&([^\[=&]+)(\[[^\]]*\])?(?:=([^&]*))?/g,
                o = {};
            url.replace(reg, function (all, name, index, value) {
                    if (index) {
                        if (!o[name]) o[name] = [];
                        o[name].push(value);
                    } else o[name] = value;
                } );
                return o;
            },
    ImageService = function (app, tagService, categoryService) {
        this.collectionName = 'images';
        this.tagService = tagService;
        this.categoryService = categoryService;

        ServiceBase.apply(this, arguments);

        var self = this;

        self.getCategories = function (err, images) {
            if (err) {
                throw err;
            }
            var group = this.group();
            images.forEach(function (item) {
                var callback = group();
                if (item.categories && item.categories.length > 0) {
                    self.categoryService.GetTags(item.categories, function (err, categories) {
                        if (err) {
                            throw err;
                        }
                        item.categories = categories;
                        callback(null, item);
                    });
                } else {
                    callback(null, item);
                }
            });
        };

        self.getTags = function (err, images) {
            if (err) {
                throw err;
            }
            var group = this.group();
            images.forEach(function (item) {
                var callback = group();
                if (item.tags && item.tags.length > 0) {
                    self.tagService.GetTags(item.tags, function (err, tags) {
                        if (err) {
                            throw err;
                        }
                        item.tags = tags;
                        callback(null, item);
                    });
                } else {
                    callback(null, item);
                }
            });
        };

        self.getItemsParam = function (err, collection) {
            var param, sort = {};
            if (err) {
                throw err;
            }
            param = this.req.params.param;
            if (param === "date") {
                sort.date = -1;
            } else if (param === "rate") {
                sort.votesAVG = -1;
            } else if (param === "count") {
                sort.votesCount = -1;
            }

            collection.find().sort(sort).toArray(this);
        }

        self.getItemsByAttr = function (err, collection) {
            var request = {}, sort = {}, p;
            if (err) {
                throw err;
            }
            if (this.req.params.tags !== "0") {
                request.tags = new BSON.ObjectID(this.req.params.tags);
            }
            if (this.req.params.category !== "0") {
                request.categories = new BSON.ObjectID(this.req.params.category);
            }
            if (request.tags) {
                request.tags = new BSON.ObjectID(request.tags);
            }
            if (request.categories) {
                request.categories = new BSON.ObjectID(request.categories);
            }
            p = this.req.params.sort;
            if (p === "1") {
                sort.title = 1 ;
            } else if (p === "2") {
                sort.title = -1 ;
            } else if (p === "3") {
                request.active = true;
            } else if (p === "4") {
                request.active = false;
            }

            collection.find(request).sort(sort).toArray(this);
        }

        self.getAllItems = function (err, collection) {
            if (err) {
                throw err;
            }

            collection.find().toArray(this);
        };

        self.getFavoritesItems = function (err, collection) {
            if (err) {
                throw err;
            }

            collection.find({favorites:true}).toArray(this);
        };

        self.deleteFavoritesF = function (err, collection) {
            var next = this;
            var IDs = parseURL("&" + this.req.params.ids);
            IDs = convertArrayToBSONArray(IDs.ids);
            collection.update({ _id: { $in: IDs } }, {$set:{favorites:false}},{ multi: true }, function (err) {
                if (err) {
                    next(err);
                    return;
                }
                next(null, true);
            });
        }

        self.download = function (req, res) {
            self.startFlow(req, res, [
                self.readFile,
                self.parseJson,
                self.getCurrentItem,

                function (err, item) {
                    if (err) {
                        throw err;
                    }

                    var fileName = './public/uploads/' + item.title;
                    if (!self.fs.existsSync(fileName)) {
                        throw "File " + item.title + " not found!";
                    }

                    this(null, fileName);
                },

                self.endFlowForceDownload
            ]);
        };

        self.deleteItem = function (err, item, collection) {
            if (err) {
                throw err;
            }
            var id = item._id.toString();
            var next = this;
            collection.remove({ "_id": new BSON.ObjectID(id) }, function (err) {
                if (err) {
                    next(err);
                    return;
                }
                fs.unlink(__dirname+"/.."+item.link, function (err) {
                    if (err) throw err;
                    console.log('successfully deleted '+__dirname+"/.."+item.link);
                });
                next(null, true);
            });

        };

        self.downloadAll = function (req, res) {
            self.startFlow(req, res, [
                self.readFile,
                self.parseJson,

                function (err, items) {
                    if (err) {
                        throw err;
                    }

                    var acceptableItems = self
                        .linq
                        .from(items)
                        .select(function (currentItem) {
                            return './public/uploads/' + currentItem.title;
                        })
                        .where(function (fileName) {
                            return self.fs.existsSync(fileName);
                        })
                        .toArray();

                    this(null, acceptableItems);
                },

                function (err, items) {
                    if (err) {
                        throw err;
                    }

                    var admZip = require('adm-zip');
                    var zip = new admZip();
                    items.forEach(function (item) {
                        zip.addLocalFile(item);
                    });

                    var next = this;
                    zip.toBuffer(function (buffer) {
                        next(null, "all-images.zip", buffer, "application/zip");
                    }, function (zipErr) {
                        next(zipErr);
                    });
                },

                self.endFlowForceDownload
            ]);
        };
    };

util.inherits(ImageService, ServiceBase);

ImageService.prototype.getIDs = function(req){
          var a = req;  
            next(null, item, collection);
    }

ImageService.prototype.getCurrentItem = function (err, collection) {
    if (err) {
        throw err;
    }

    var name = this.req.params.name,
        next = this;
    //     id = parseInt(id,10);
    if (!name) {
        this("Name is not set.");
    }

    collection.findOne({ "name": name }, function (err, item) {
        if (err) {
            next(err);
        }
        if (item) {
            next(null, item, collection);
            return;
        }
        next("Item with name " + name + " doesn't exist.");
    });
};


ImageService.prototype.mapRequestItem = function (req, res, item, collection, next) {
    //   item.name = req.body.name;
    var checkForHexRegExp = new RegExp("^[0-9a-fA-F]{24}$");
    item.name = req.body.name;
    item.votes = req.body.votes;
    if (typeof item.active === 'undefinied' && typeof req.body.active === 'undefinied') {
        item.active = true;
    } else {
        item.active = req.body.active;
    }
    if (typeof item.favorites === 'undefinied' && typeof req.body.favorites === 'undefinied') {
        item.favorites = false;
    } else {
        item.favorites = req.body.favorites;
    }
    if (typeof item.votes === 'undefinied') {
        item.votes = [];
        item.votesAVG = 0;
        item.votesCount = 0;
    } else {
        item.votesAVG = avgFunction(item.votes);
        item.votesCount = item.votes.length;
    }
    if (item.tags.length && item.tags[0]._id === checkForHexRegExp)
        item.tags = convertArrayToBSONArray(req.body.tags);
    if (item.categories.length && item.categories[0]._id === checkForHexRegExp)
        item.categories = convertArrayToBSONArray(req.body.categories);

    next(null, item, collection);
};

ImageService.prototype.mapNewRequestItem = function (req, res, item, collection, next) {

    var self = this;
    item.date = new Date();
    item.title = req.body.title;
    item.name = req.files.file.name;
    if (typeof req.body.tags === 'string')
        item.tags = JSON.parse(req.body.tags);
    if (typeof req.body.categories === 'string')
        item.categories = JSON.parse(req.body.categories);


    if (!item.categories || item.categories.length < 1) {
        this("Category not set");
    }
    if (!item.tags || item.tags.length < 1) {
        this("Tag not set");
    }
    if (!item.name) {
        this("Name not set");
    }
    
    item.filePath = "./public/uploads/"+item.name;
    item.tags = convertArrayToBSONArray(item.tags);
    item.categories = convertArrayToBSONArray(item.categories);

    if (typeof (item.active) === 'undefined') {
        item.active = true;
    }
    if (typeof (item.favorites) === 'undefined') {
        item.favorites = false;
    }
    if (typeof (item.link) === 'undefined') {
      //  item.link = "/content/images/gallery/thumbs/0"+Math.floor((Math.random()*9)+1)+".jpg";
        item.link = "/uploads/"+item.name;
    }
    if (typeof item.votes !== 'undefinied') {
        item.votes = [];
        item.votesAVG = 0;
    }
    next(null, item, collection);
};
ImageService.prototype.validateUpdate = function (req, item, collection, next) {
    var self = this;

    if (!item.name) {
        next("Image name must be set.");
        return;
    }
    next(null, item, collection);

}
ImageService.prototype.validate = function (req, item, collection, next) {
    var self = this;
    if (item.name) {
        
        if (!req.files
            || !req.files.file
            || !req.files.file.path
            || !req.files.file.originalFilename
            || !self.fs.existsSync(req.files.file.path)) {
            next("Failed to upload a file.");
            return;
        }

        collection.find({
            "name": item.name
        }).count(function (err, count) {
            if (count > 0) {
                next("Image " + item.name + " already exists.");
                return;
            }
            next(null, item, collection);
        });
        return;
    }

    next(null, item, collection);
};

ImageService.prototype.beforeSave = function (req, item, collection, next) {
    var self = this;

    if (req.files && req.files.file && req.files.file.path && req.files.file.originalFilename) {
        self.step(
            function () {
                self.fs.exists(item.filePath, this);
            },

            function (exists) {
                if (exists) {
                    self.fs.unlink(item.filePath, this);
                } else {
                    this(null);
                }
            },

            function (err) {
                if (err) {
                    throw err;
                }

                self.fs.rename(req.files.file.path, item.filePath, this);
            },

            function (err) {
                if (err) {
                    next(err);
                } else {
                    delete item.filePath;

                    next(null, item, collection);
                }
            }
        );
    } else {
        next(null, item, collection);
    }
};

ImageService.prototype.listByAttr = function (req, res) {
    var self = this;

    self.startFlow(req, res, [
	     self.getCollection,
         self.getItemsByAttr,
         self.getTags,
         self.getCategories,
        self.endFlow
    ]);
};

ImageService.prototype.listParam = function (req, res) {
    var self = this;

    self.startFlow(req, res, [
	     self.getCollection,
         self.getItemsParam,
         self.getTags,
         self.getCategories,
        self.endFlow
    ]);
};


ImageService.prototype.listFavorites = function (req, res) {
    var self = this;

    self.startFlow(req, res, [
	     self.getCollection,
         self.getFavoritesItems,
         self.getTags,
         self.getCategories,
        //self.getAllItems,
        self.endFlow
    ]);
};

ServiceBase.prototype.deleteFavorites = function(req, res) {
    var self = this;

    self.startFlow(req, res, [
        self.getCollection,
        self.deleteFavoritesF,
        self.endFlow
    ]);
};

ImageService.prototype.list = function (req, res) {
    var self = this;

    self.startFlow(req, res, [
	     self.getCollection,
         self.getAllItems,
         self.getTags,
         self.getCategories,
        //self.getAllItems,
        self.endFlow
    ]);
};


module.exports = ImageService;