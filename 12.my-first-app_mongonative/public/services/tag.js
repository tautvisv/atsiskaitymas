﻿var util = require("util"),
    BSON = require('mongodb').BSONPure,
    ServiceBase = require('./base'), 

    TagService = function(app) {
        var self = this;
        this.collectionName = 'tags';
        
        this.GetTags = function (ids, callback) {
            self.getDatabase().collection(self.collectionName, function (err, collection) {
                collection.find({ "_id": { $in: ids } },{name:1}).toArray(callback);
            });

        };

        ServiceBase.apply(this, arguments);
    };

util.inherits(TagService, ServiceBase);

TagService.prototype.getCurrentItem = function(err, collection) {
        if (err) {
            throw err;
        }

        var name = this.req.params.name,
            next = this;
       //     id = parseInt(id,10);
        if (!name) {
            this("Name is not set.");
        }

        collection.findOne({"name": name }, function(err, item) {
            if (err) {
                next(err);
            }
            if (item) {
                next(null, item, collection);
                return;
            }
            next("Item with name " + name + " doesn't exist.");
        });
    };

TagService.prototype.mapNewRequestItem = function (req, res, item, collection, next) {
    item.name = req.body.name;
    item.images = [];
    next(null, item, collection);
}

TagService.prototype.mapRequestItem = function(req, res, item, collection, next) {
    item.name = req.body.name;
    next(null, item, collection);
};

TagService.prototype.validate = function(req, item, collection, next) {
    var self = this;

    if (!item.name) {
        next("Tag name must be set.");
        return;
    }

    collection.find({
        "name": item.name
    }).count(function(err, count) {
        if (err) {
            next(err);
            return;
        }

        if (count > 0) {
            next("Tag " + item.name + " already exists.");
            return;
        }

        next(null, item, collection);
    });
};

TagService.prototype.afterUpdate = function(collection, item, next) {
    var self = this;

    self.app.events.emit('TagUpdated', item);

    next(null, item, collection);
};

module.exports = TagService;